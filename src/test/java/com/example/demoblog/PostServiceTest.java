package com.example.demoblog;

import com.example.demoblog.domain.PostEntity;
import com.example.demoblog.repository.PostRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertFalse;


/**
 * @author leeseungmin on 2019-04-19
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class PostServiceTest {
  @Autowired
  private PostRepository postRepository;

  @Before
  public void setup(){
    postRepository.save(PostEntity.builder().subject("제목").contents("내용").build());
  }

  @Test
  public void getDataAll(){
    assertFalse(postRepository.findAll().isEmpty());
  }
}
