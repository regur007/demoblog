package com.example.demoblog;

import com.example.demoblog.domain.PostEntity;
import com.example.demoblog.service.PostService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author leeseungmin on 2019-04-19
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PostControllerTest {
  private ObjectMapper objectMapper;
  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private PostService postService;

  @Before
  public void setup(){
    assertNotNull(postService);
    objectMapper = new ObjectMapper();
    objectMapper.configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID,false);
    objectMapper.registerModule(new JavaTimeModule());
  }

  @Test
  public void test() throws Exception{
    //쓰기 테스트
    this.mockMvc.perform(post("/post")
      .content(objectMapper.writeValueAsString(PostEntity.builder().subject("제목").contents("내용").build()))
      .contentType(MediaType.parseMediaType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    )
      .andExpect(content().contentType(MediaType.parseMediaType(MediaType.APPLICATION_JSON_UTF8_VALUE)))
      .andExpect(status().isOk());

    List<PostEntity> posts = postService.getPostList();

    //목록 테스트
    this.mockMvc.perform(get("/post"))
      .andExpect(content().contentType(MediaType.parseMediaType(MediaType.APPLICATION_JSON_UTF8_VALUE)))
      .andExpect(content().json(objectMapper.writeValueAsString(posts)))
      .andExpect(status().isOk());

    PostEntity post = postService.getPost(1);

    //보기 테스트
    this.mockMvc.perform(get("/post/{id}",1))
      .andExpect(content().contentType(MediaType.parseMediaType(MediaType.APPLICATION_JSON_UTF8_VALUE)))
      .andExpect(content().json(objectMapper.writeValueAsString(posts)))
      .andExpect(status().isOk());

    this.mockMvc.perform(delete("/post/{id}",1))
      .andExpect(status().isOk());
    assertTrue(postService.getPostList().isEmpty());
  }

}
