package com.example.demoblog.controller;

import com.example.demoblog.domain.PostEntity;
import com.example.demoblog.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author leeseungmin on 2019-04-19
 */
@RestController
@RequestMapping("post")
public class PostController {

  @Autowired
  private PostService postService;

  @GetMapping("")
  public List<PostEntity> list(){
    return postService.getPostList();
  }

  @GetMapping("/{id}")
  public PostEntity view(@PathVariable("id") long id){
    return postService.getPost(id);
  }

  @PostMapping("")
  public PostEntity post(@RequestBody PostEntity post) {
    return postService.save(post);
  }

  @DeleteMapping("/{id}")
  public void delete(@PathVariable("id") long id) {
    postService.delete(id);
  }
}
