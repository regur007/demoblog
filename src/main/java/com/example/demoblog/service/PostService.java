package com.example.demoblog.service;

import com.example.demoblog.domain.PostEntity;
import com.example.demoblog.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

/**
 * @author leeseungmin on 2019-04-19
 */
@Service
@Transactional
public class PostService {
  private PostRepository postRepository;

  @Autowired
  public void setPostRepository(PostRepository postRepository){
    this.postRepository = postRepository;
  }

  public List<PostEntity> getPostList(){
    return Collections.unmodifiableList(postRepository.findAll());
  }
  public PostEntity save(PostEntity postEntity) {
    return postRepository.save(postEntity);
  }

  public PostEntity getPost(long id) {
    return postRepository.findById(id).get();
  }

  public void delete(long id) {
    postRepository.deleteById(id);
  }
}
