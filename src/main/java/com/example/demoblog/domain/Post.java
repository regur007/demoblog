package com.example.demoblog.domain;

/**
 * @author leeseungmin on 2019-04-19
 */
public interface Post {
  Long getId();

  String getSubject();

  String getContents();

  java.time.LocalDateTime getCreationDateTime();

  java.time.LocalDateTime getModificationDateTime();
}
