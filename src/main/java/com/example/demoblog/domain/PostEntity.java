package com.example.demoblog.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author leeseungmin on 2019-04-19
 */
@Entity
@Table(name = "POST")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Setter(AccessLevel.NONE)
@Builder
public class PostEntity implements Post {
  @Id
  @SequenceGenerator(name = "POST_ID_GEN",sequenceName = "POST_ID_SEQ",allocationSize = 1)
  @GeneratedValue(generator = "POST_ID_GEN", strategy = GenerationType.SEQUENCE)
  @Column(nullable = false)
  private Long id;
  @Column(nullable = false, length = 255)
  private String subject;
  @Column
  @Lob
  private String contents;
  @Column(name = "CREATION_DATETIME",nullable = false)
  @CreationTimestamp
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
  private LocalDateTime creationDateTime;
  @Column
  private LocalDateTime modificationDateTime;
}
