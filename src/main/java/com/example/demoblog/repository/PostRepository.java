package com.example.demoblog.repository;

import com.example.demoblog.domain.PostEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author leeseungmin on 2019-04-19
 */
public interface PostRepository extends JpaRepository<PostEntity, Long> {
}
